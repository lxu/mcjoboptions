## JIRA ticket

ATLMCPROD-**please fill this**

## Checklist for requesters

- [ ] I have followed the [guidelines in McSampleRequestProcedure](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/McSampleRequestProcedure)
- [ ] I have provided the ATLMCPROD JIRA ticket where the request has been discussed
- [ ] The request has been approved by the physics group
- [ ] I have checked the "Squash commits when merge request is accepted" option below
- [ ] I have checked the "Delete source branch when merge request is accepted." option below

## Checklist for approvers

- [ ] All the items in the "Checklist for requesters" have been completed successfully
- [ ] The pipeline has run and the status is green
- [ ] CI jobs have not been skipped (if jobs have been skipped, the requesters should provide a reason for skipping and confirmation from the PMG conveners is necessary before merging)
- [ ] Look at the output of the `run_athena` pipeline and make sure that it has run **for one of the DSIDs added to the commit**. If it's not the case, confirmation from the PMG conveners is necessary before merging
- [ ] Check that no `log.generate.short` files are included in the commit to be merged to master. If such files are present, it indicates that something went wrong in the pipeline. Check the job logs and contact the package maintainers if needed.
- [ ] Check that no file has been modified or deleted
- [ ] Check that the title of the MR is descriptive enough (if not ask the requester to modify it)

/label ~jobOptions
/assign @joany @cbecot
