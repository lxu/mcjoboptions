# based 429705
evgenConfig.description = "pp->Jpsi(mumu) + Z->mumu via second hard process"
evgenConfig.keywords = ["charmonium","Jpsi","Z","4muon","SM"]
evgenConfig.contact = ["Ketevi Adikle Assamagan <ketevi.adikle.assamagan@cern.ch>"]
evgenConfig.process = "pp -> Jpsi Z -> mu+-mu-+ mu+-mu-+"
evgenConfig.minevents = 5000

include('Pythia8B_i/Pythia8B_A14_NNPDF23LO_Common.py') 
include("Pythia8B_i/Pythia8B_Charmonium_Common.py")

genSeq.Pythia8B.Commands += ['443:onMode = off']
genSeq.Pythia8B.Commands += ['443:2:onMode = on']
genSeq.Pythia8B.Commands += ['23:onMode = off']
genSeq.Pythia8B.Commands += ['23:7:onMode = on']
genSeq.Pythia8B.Commands += ['SecondHard:generate = on']
genSeq.Pythia8B.Commands += ['SecondHard:SingleGmZ = on']
genSeq.Pythia8B.SignalPDGCodes = [443,-13,13]

genSeq.Pythia8B.TriggerPDGCode = 13
genSeq.Pythia8B.TriggerStatePtCut = [20.0]
genSeq.Pythia8B.TriggerStateEtaCut = 2.5
genSeq.Pythia8B.MinimumCountPerCut = [1]

