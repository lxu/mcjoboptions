#based on 361224
evgenConfig.description = "Low-pT inelastic minimum bias events using EPOS"
evgenConfig.keywords = ["QCD", "minBias" , "SM"]
evgenConfig.contact  = [ "deepak.kar@cern.ch" ]
evgenConfig.minevents = 1000

include("Epos_i/Epos_Base_Fragment.py")
