include("Sherpa_i/2.2.8_NNPDF30NNLO.py")

evgenConfig.description = "Sherpa 2.2.8 ttbar production with tt+0,1j@NLO(QCD+EWvirt)+2,3,4j@LO in the dlepton channel."
evgenConfig.keywords = ["SM", "top", "tttt" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "frank.siegert@cern.ch" ]
evgenConfig.minevents = 1000

genSeq.Sherpa_i.RunCard="""
(run){
  SCALES VAR{H_T2/16}
  EXCLUSIVE_CLUSTER_MODE 1;

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops;
  CSS_REWEIGHT=1
  REWEIGHT_SPLITTING_ALPHAS_SCALES 1
  REWEIGHT_SPLITTING_PDF_SCALES 1
  CSS_REWEIGHT_SCALE_CUTOFF=5.0
  HEPMC_INCLUDE_ME_ONLY_VARIATIONS=1

  NLO_CSS_PSMODE=1
  INTEGRATION_ERROR=0.05;

  %decay settings
  HARD_DECAYS On; HARD_SPIN_CORRELATIONS 1;
  STABLE[24] 0; STABLE[6] 0; WIDTH[6] 0;
}(run)

(processes){
  Process : 93 93 ->  6 -6 6 -6;
  NLO_QCD_Mode 3;
  ME_Generator Amegic;
  RS_ME_Generator Comix;
  Loop_Generator LOOPGEN;
  Order (*,0);
  End process
}(processes)
"""

genSeq.Sherpa_i.Parameters += [ "WIDTH[6]=0" ]
genSeq.Sherpa_i.Parameters += [ "OL_PREFIX=./Process/OpenLoops" ]

genSeq.Sherpa_i.NCores = 24
genSeq.Sherpa_i.OpenLoopsLibs = [ "pptttt" ]
genSeq.Sherpa_i.CleanupGeneratedFiles = 1
