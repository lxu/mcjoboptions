from MadGraphControl.MadGraphUtils import *

nevents=10000
mode=0
gridpack_dir='madevent/'
ptgmin=130

mgproc="""generate p p > vl vl~ a j j QCD^2==2"""
name='nunugamma2jINT'
process='pp>nunua'
keyword=['SM','Z','photon','diboson','VBS','QCD']
description = 'MadGraph Z->nunug plus two jets EWK-QCD interference'

print("Debug::I have choosen process:"+name)
#---------------------------------------------------------------------------
# MG5 Proc card
#---------------------------------------------------------------------------
fcard = open('proc_card_mg5.dat','w')
fcard.write("""
import model sm-no_b_mass
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
"""+mgproc+"""
output -f
""")
fcard.close()

#----------------------------------------------------------------------------
# Random Seed
#----------------------------------------------------------------------------
randomSeed = 0
if hasattr(runArgs,'randomSeed'): randomSeed = runArgs.randomSeed

#----------------------------------------------------------------------------
# Beam energy
#----------------------------------------------------------------------------
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = int(runArgs.ecmEnergy) / 2.
else:
    raise RunTimeError("No center of mass energy found.")

#---------------------------------------------------------------------------
# Number of Events
#---------------------------------------------------------------------------
safefactor = 1.1
if hasattr(runArgs,'maxEvents') and runArgs.maxEvents > 0:  nevents = int(int(runArgs.maxEvents)*safefactor)
else: nevents = int(nevents*safefactor)

#---------------------------------------------------------------------------
# Number of Events
#---------------------------------------------------------------------------
skip_events=0
if hasattr(runArgs,'skipEvents'): skip_events=runArgs.skipEvents

#---------------------------------------------------------------------------
# MG5 Run Card
#---------------------------------------------------------------------------
extras = { 'lhe_version' : '3.0',
           'cut_decays'  : 'T', 
           'pdlabel'     :"'lhapdf'",
           'lhaid'       :"260000",
           'dynamical_scale_choice':'3',
           'auto_ptj_mjj': 'F',
           'ptj':"15",
           'ptb':"15",
           'pta':"10",
           'ptl':"10",
           'etaj':"5.5",
           'etab':"5.5",
           'etal':"3.0",
           'etaa':"3.0",
           'drjj':"0.1",
           'drll':"0.1",
           'draa':"0",
           'draj':"0.1",
           'drjl':"0.1",
           'dral':"0.1",
           'mmjj':"0",
           'mmll':"40",
           'ptgmin'      : ptgmin,
           'epsgamma':'0.1',
           'R0gamma' :'0.1', 
           'xn'    :'2',
           'isoEM' :'True', 
           'bwcutoff'    :'15',
           'maxjetflavor': '5',
           'asrwgtflavor': '5', 
           'use_syst'     : 'True',
           'systematics_program':'systematics',
           'systematics_arguments':"['--mur=0.5,1,2', '--muf=0.5,1,2', '--dyn=-1,3', '--pdf=errorset,13100@0,25200@0,265000@0,266000@0']" }
           

process_dir = new_process(grid_pack=gridpack_dir)
build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),
               run_card_new='run_card.dat', 
               nevts=nevents,
               rand_seed=randomSeed,
               beamEnergy=beamEnergy,
               extras=extras)

print_cards()


#---------------------------------------------------------------------------
# MG5 + Pythia8 setup and process (lhe) generation
#---------------------------------------------------------------------------
generate(run_card_loc='run_card.dat',param_card_loc=None,mode=mode,proc_dir=process_dir,run_name=name,grid_pack=False,gridpack_dir=gridpack_dir,nevents=nevents,random_seed=runArgs.randomSeed)


#--------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------

outDS = arrange_output(run_name=name,
               proc_dir=process_dir,
               outputDS=name+'._00001.tar.gz',
               lhe_version=3,
               saveProcDir=True
               )


#### Shower
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
include("Pythia8_i/Pythia8_ShowerWeights.py")
genSeq.Pythia8.Commands += [
            "SpaceShower:dipoleRecoil = on"]

evgenConfig.generators = ["MadGraph", "Pythia8", "EvtGen"]

evgenConfig.contact = ['Evgeny Soldatov <Evgeny.Soldatov@cern.ch>']
evgenConfig.keywords+=keyword
evgenConfig.description = description

runArgs.inputGeneratorFile = outDS

import os
if 'ATHENA_PROC_NUMBER' in os.environ:
   print 'Noticed that you have run with an athena MP-like whole-node setup. Will re-configure now to make sure that the remainder of the job runs serially.'
   njobs = os.environ.pop('ATHENA_PROC_NUMBER')
   # Try to modify the opts underfoot
   if not hasattr(opts,'nprocs'): mglog.warning('Did not see option!')
else:
   opts.nprocs = 0
   print opts
