evgenConfig.inputconfcheck="sbi_4l"
evgenConfig.nEventsPerJob = 2000

proc_name="VBF4l_SBI"
m4lmin=None
m4lmax="130"

include("MadGraphControl_Pythia8EvtGen_lllljj_EW6.py")
