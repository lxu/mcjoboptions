evgenConfig.inputconfcheck="sig_4l"
evgenConfig.nEventsPerJob = 5000

proc_name="VBF4l_S"
m4lmin="130"
m4lmax=None

include("MadGraphControl_Pythia8EvtGen_lllljj_EW6.py")
