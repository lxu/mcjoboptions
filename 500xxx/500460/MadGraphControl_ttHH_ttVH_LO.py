from MadGraphControl.MadGraphUtils import *

# General settings
minevents=5000
nevents = 1.1*minevents
mode=0

if process=="ttHH":
    mgproc="""generate p p > t t~ h h"""
    name="ttbarHH"
    keyword=['SM','top','ttVV']
elif process=="ttWH":
    mgproc="""generate p p > t t~ w h"""
    name="ttbarWH"
    keyword=['SM','top','ttVV']
else: 
    raise RuntimeError("process not found")

stringy = 'madgraph.'+str(runArgs.jobConfig)[-8:-2]+'.MadGraph_'+str(name)

fcard = open('proc_card_mg5.dat','w')
fcard.write("""
import model sm
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define w = w+ w-
"""+mgproc+"""
output -f
""")
fcard.close()


beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")


pdflabel="nn23lo1"


#Fetch default LO run_card.dat and set parameters
extras = { 'lhe_version'  : '2.0',
           'pdlabel'      : "'"+pdflabel+"'" }

process_dir = new_process()
build_run_card(run_card_old=get_default_runcard(process_dir),run_card_new='run_card.dat', 
               nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,
               extras=extras)

print_cards()
generate(run_card_loc='run_card.dat',param_card_loc=None,mode=mode,proc_dir=process_dir)
arrange_output(proc_dir=process_dir,outputDS=stringy+'._00001.events.tar.gz')


#### Shower 
evgenConfig.generators += ["MadGraph", "Pythia8"]
evgenConfig.contact = ["nedaa.asbah@cern.ch"]
evgenConfig.description = 'MadGraph_'+str(name)
evgenConfig.keywords+=keyword 
evgenConfig.inputfilecheck = stringy
evgenConfig.minevents = minevents
runArgs.inputGeneratorFile=stringy+'._00001.events.tar.gz'

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

#include("Pythia8_i/Pythia8_SMHiggs125_inc.py")
#To test The Higgs decay part is added here

genSeq.Pythia8.Commands += [ '25:onMode = off',
                             '25:oneChannel = 1 0.5770   100 5 -5',
                             '25:addChannel = 1 0.0291   100 4 -4',
                             '25:addChannel = 1 0.000246 100 3 -3',
                             '25:addChannel = 1 0.00000  100 6 -6',
                             '25:addChannel = 1 0.000219 100 13 -13',
                             '25:addChannel = 1 0.0632   100 15 -15',
                             '25:addChannel = 1 0.0857   100 21 21',
                             '25:addChannel = 1 0.00228  100 22 22',
                             '25:addChannel = 1 0.00154  100 22 23',
                             '25:addChannel = 1 0.0264   100 23 23',
                             '25:addChannel = 1 0.2150   100 24 -24'
]
