# Multicharges.py
# by Yury.Smirnov@cern.ch

from MadGraphControl.MadGraphUtils import *

if hasattr(runArgs, 'ecmEnergy'):
	beamEnergy = runArgs.ecmEnergy / 2.
else:
	raise RuntimeError("No center-of-mass energy provided in the Gen_tf.py command, exiting now.")


PdgId = 10000000+chargePoint*100

run_card_extras = {}

if not isPF:	# MC16, DY
	print 'DY, mass is', massPoint, 'GeV, charge is +/-', chargePoint, 'e'
	evgenConfig.description="Drell-Yan multi-charged particles generation for mass = %i GeV, charge = +/- %ie with MadGraph5_aMC@NLO+Pythia8, NNPDF23LO pdf and A14 tune in MC16" % (massPoint, chargePoint)
	runName = 'MGPy8.Multicharges_DY_M%iZ%ip0' % (massPoint, chargePoint)
	process_str="import model multichargedParticles\ngenerate p p > qb%ip0 qb%ip0~\noutput -f" % (chargePoint, chargePoint)
	evgenConfig.keywords += ['exotic','BSM','longLived','drellYan']
	run_card_extras['lhaid'] = '247000' # NNDF23_lo_as_0130_qed pdf set
	include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
else:	# MC16, PF
	print 'PF, mass is', massPoint, 'GeV, charge is +/-', chargePoint, 'e'
	evgenConfig.description="Photon-fusion multi-charged particles generation for mass = %i GeV, charge = +/- %ie with MadGraph5_aMC@NLO+Pythia8, LUXqed pdf and A14 tune in MC16" % (massPoint, chargePoint)
	runName = 'MGPy8.Multicharges_PF_M%iZ%ip0' % (massPoint, chargePoint)
	process_str="import model multichargedParticles\ngenerate a a > qb%ip0 qb%ip0~\noutput -f" % (chargePoint, chargePoint)
	evgenConfig.keywords += ['exotic','BSM','longLived']
	run_card_extras['lhaid'] = '82200' # LUXqed17_plus_PDF4LHC15_nnlo_100 pdf set
	get_Pythia8A14LUXqedEvtGenCommonFile = subprocess.Popen(['get_files', '-jo', 'Pythia8_A14_LUXqed_EvtGen_Common.py'])
	if get_Pythia8A14LUXqedEvtGenCommonFile.wait():
		print "Could not get hold of Pythia8_A14_LUXqed_EvtGen_Common.py, exiting..."
		sys.exit(2)
	include("Pythia8_A14_LUXqed_EvtGen_Common.py")


run_card_extras['pdlabel'] = 'lhapdf'
run_card_extras['lhe_version'] = '2.0'

process_dir = new_process(process_str)

modify_run_card(process_dir=process_dir, runArgs=runArgs, settings=run_card_extras)

param_card_extras = { "MULTICHARGEDPARTICLES": { 'MQb%ip0' %(chargePoint): massPoint}} # changing mass in the param_card
modify_param_card(process_dir=process_dir, params=param_card_extras)

print_cards()

generate(process_dir=process_dir, grid_pack=False, runArgs=runArgs)

arrange_output(process_dir=process_dir, runArgs=runArgs, lhe_version=2, saveProcDir=True)

evgenConfig.contact = ['Yury.Smirnov@cern.ch']

evgenConfig.specialConfig = "MASS=%i;CHARGE=%i;preInclude=SimulationJobOptions/preInclude.Qball.py;MDT_QballConfig=True;InteractingPDGCodes=[%i,%i]" % (massPoint, chargePoint, PdgId, (-1)*PdgId)

# Edit PDGTABLE.MeV with MCPs mass

ALINE1="M %i                         %i.E+03       +0.0E+00 -0.0E+00 qb%ip0             +" % (PdgId, massPoint, chargePoint)
ALINE2="W %i                         0.E+00         +0.0E+00 -0.0E+00 qb%ip0              +" % (PdgId, chargePoint)

import os, sys

pdgmod = os.path.isfile('PDGTABLE.MeV')
if pdgmod is True:
	os.remove('PDGTABLE.MeV')
os.system('get_files -data PDGTABLE.MeV')
f=open('PDGTABLE.MeV','a')
f.writelines(str(ALINE1))
f.writelines('\n')
f.writelines(str(ALINE2))
f.writelines('\n')
f.close()

del ALINE1
del ALINE2

# Edit G4particle_whitelist.txt with MCPs

ALINE3="%i   qb%ip0  %i (Mev/c) lepton 0" %(PdgId, chargePoint, massPoint*1000)
ALINE4="%i   qb%ip0bar  %i (Mev/c) lepton 0" %((-1)*PdgId, chargePoint, massPoint*1000)

pdgmod = os.path.isfile('G4particle_whitelist.txt')
if pdgmod is True:
	os.remove('G4particle_whitelist.txt')
os.system('get_files -data G4particle_whitelist.txt')
f=open('G4particle_whitelist.txt','a')
f.writelines(str(ALINE3))
f.writelines('\n')
f.writelines(str(ALINE4))
f.writelines('\n')
f.close()

del ALINE3
del ALINE4

include("Pythia8_i/Pythia8_MadGraph.py")

# These two lines below fix an issue with Pythia 8.205 not recognizing multi-charged particles. For details please check the https://groups.cern.ch/group/hn-atlas-Generators/Lists/Archive/Flat.aspx?RootFolder=%2Fgroup%2Fhn-atlas-Generators%2FLists%2FArchive%2FError%20in%20Pythiacheck%20unknown%20particle%20code&FolderCTID=0x012002006E9F14B8795719469C62A1525BB20B42
exec('genSeq.Pythia8.Commands += [\"'+str(int(PdgId))+':all = qb'+str(chargePoint)+'p0 qb'+str(chargePoint)+'p0bar 2 '+str(chargePoint*3)+' 0 '+str(massPoint)+'\"]')
exec('genSeq.Pythia8.Commands += [\"-'+str(int(PdgId))+':all = qb'+str(chargePoint)+'p0bar qb'+str(chargePoint)+'p0 2 '+str(chargePoint*(-3))+' 0 '+str(massPoint)+'\"]')
