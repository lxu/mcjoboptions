#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Herwig7+EvtGen single-top-quark s-channel production (anti-top), inclusive, H7.1-Default tune, ME NNPDF30 NLO'
evgenConfig.keywords    = [ 'SM', 'top', 'singleTop', 'sChannel', 'inclusive' ]
evgenConfig.contact     = [ 'serena.palazzo@cern.ch' ]
evgenConfig.generators += [ 'Powheg','Herwig7', 'EvtGen' ]
evgenConfig.nEventsPerJob    = 100000
evgenConfig.inputfilecheck = 'TXT'
evgenConfig.tune = "H7.1-Default"

#--------------------------------------------------------------
# Herwig7 showering with the H7UE MMHT2014 tune
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering of LHE files
include("Herwig7_i/Herwig7_LHEF.py")
include("Herwig7_i/Herwig7_701_StripWeights.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.tune_commands()
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")

# add EvtGen
include("Herwig7_i/Herwig71_EvtGen.py")


# run Herwig7
Herwig7Config.run()

