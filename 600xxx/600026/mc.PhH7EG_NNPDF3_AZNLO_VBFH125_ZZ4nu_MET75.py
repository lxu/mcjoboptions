evgenConfig.process          = "VBF->H, H->inv"
evgenConfig.description      = "POWHEG+Herwig7 VBF H->ZZ->4nu production"
evgenConfig.keywords         = [ "SM", "Higgs", "SMHiggs", "mH125" ]
evgenConfig.contact          = [ 'philipp.mogg@cern.ch', "vdao@cern.ch"  ]
evgenConfig.nEventsPerJob    = 10000
evgenConfig.inputFilesPerJob = 10
evgenConfig.generators       = [ 'Powheg', 'Herwig7', 'EvtGen' ]
evgenConfig.tune             = "H7.1-Default"
evgenConfig.inputfilecheck   = "TXT"

# initialize Herwig7 generator configuration for showering of LHE files
include("Herwig7_i/Herwig7_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")
Herwig7Config.tune_commands()

# add EvtGen
include("Herwig7_i/Herwig71_EvtGen.py")

# only consider H->inv
Herwig7Config.add_commands("""
do /Herwig/Particles/h0:SelectDecayModes h0->Z0,Z0;
# print out decays modes and branching ratios
do /Herwig/Particles/h0:PrintDecayModes
do /Herwig/Particles/Z0:SelectDecayModes Z0->nu_e,nu_ebar; Z0->nu_mu,nu_mubar; Z0->nu_tau,nu_taubar;
do /Herwig/Particles/Z0:PrintDecayModes
""")

# run Herwig7
Herwig7Config.run()

# Missing Et filter
include('GeneratorFilters/MissingEtFilter.py')
filtSeq.MissingEtFilter.METCut = 75*GeV
filtSeq.Expression = "MissingEtFilter" 
