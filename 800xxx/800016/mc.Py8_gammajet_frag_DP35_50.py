include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_ReducedShowerWeights.py")

evgenConfig.description = "Pythia8 dijet events with prompt (fragmentation) photons in 35 < pT_ylead < 50."
evgenConfig.keywords = ["egamma", "performance", "jets", "photon", "QCD"]
evgenConfig.nEventsPerJob = 50
evgenConfig.contact = ["frank.siegert@cern.ch", "ana.cueto@cern.ch"]

## Configure Pythia
genSeq.Pythia8.Commands += ["HardQCD:gg2gg = on",
                            "HardQCD:gg2qqbar = on",
                            "HardQCD:qg2qg = on",
                            "HardQCD:qq2qq = on",
                            "HardQCD:qqbar2gg = on",
                            "HardQCD:qqbar2qqbarNew = on",
                            "PhaseSpace:pTHatMin = 17.5",
]
##pTHatMin is set to Ptmin/2 to ensure an unbiased spectrum

genSeq.Pythia8.UserHooks = ["EnhanceSplittings"]
genSeq.Pythia8.Commands += [
                            "EnhanceSplittings:isr:Q2QA = 100",
                            "EnhanceSplittings:fsr:Q2QA = 100",
]

include("GeneratorFilters/DirectPhotonFilter.py")
filtSeq.DirectPhotonFilter.NPhotons = 1
filtSeq.DirectPhotonFilter.Ptmin = [ 35000. ]
filtSeq.DirectPhotonFilter.Ptmax = [ 50000. ]
filtSeq.DirectPhotonFilter.OrderPhotons = True
