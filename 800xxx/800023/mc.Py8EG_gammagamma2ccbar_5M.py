
evgenConfig.description = "Pythia8 gamma-gamma to c-cbar with Pb+Pb photon fluxes"
evgenConfig.keywords = ["QED"]
evgenConfig.contact     = [ 'mateusz.dyndal@cern.ch']
evgenConfig.nEventsPerJob   = 10000

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

# This AthenaTool will be used to set the Photon Flux
# =====================================================
# You can find the code in Generators/Pythia8_i/src/Pythia8Custom/
from Pythia8_i.Pythia8_iConf import UserPhotonFlux
photonFluxTool=UserPhotonFlux()
photonFluxTool.Process=3
photonFluxTool.MinimumB=6.636
from AthenaCommon.AppMgr import ToolSvc
ToolSvc += UserPhotonFlux()
genSeq.Pythia8.CustomInterface = photonFluxTool

# This is normal JobOptions stuff
# ===============================
genSeq.Pythia8.Beam1='ELECTRON'
genSeq.Pythia8.Beam2='ELECTRON'
genSeq.Pythia8.Commands += [
"PDF:lepton2gamma = on",
"PDF:lepton2gammaSet = 2",
"PDF:lepton2gammaApprox = 2",
"Photon:Wmin  = 5.",
"Photon:sampleQ2 = off",
"HardQCD:all = off",
"PhotonCollision:gmgm2ccbar = on",
"PhaseSpace:pTHatMin = 2.0",
"ParticleDecays:allowPhotonRadiation = on",
]
