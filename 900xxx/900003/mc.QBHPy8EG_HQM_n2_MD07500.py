evgenConfig.description = "QBH in the Horizon Quantum Mechanics model (n = 2, MD = 7.5 TeV) A14_CTEQ6L1"
evgenConfig.process = "QBH -> 2-body"
evgenConfig.keywords = ["BSM", "exotic", "blackhole", "extraDimensions", "ADD"]
evgenConfig.generators += ["QBH"]
evgenConfig.contact = ["Doug Gingrich <gingrich@ualberta.ca>"]
evgenConfig.inputfilecheck = "HQM"
evgenConfig.nEventsPerJob = 10000

include("Pythia8_i/Pythia8_A14_CTEQ6L1_EvtGen_Common.py" )
include("Pythia8_i/Pythia8_LHEF.py")

# Increase tolerance on displaced vertieces due to highly boosted heavy flavours.
testSeq.TestHepMC.MaxVtxDisp = 1000*1000 #in mm
testSeq.TestHepMC.MaxTransVtxDisp = 1000*1000 #in mm
