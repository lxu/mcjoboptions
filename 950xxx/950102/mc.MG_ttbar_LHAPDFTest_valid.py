from MadGraphControl.MadGraphUtils import *

nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob

process = """
import model sm
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
generate p p > t t~
output -f"""

process_dir = new_process(process)

#Fetch default LO run_card.dat and set parameters
settings = {'lhe_version':'2.0', 
          'cut_decays' :'F', 
          'pdlabel'    : "'lhapdf'",
          'lhaid'      : 247000,
          'sys_pdf'    : 'NNPDF23_lo_as_0130_qed',
          'nevents'    : nevents}
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

############################
# Shower JOs will go here
evgenConfig.generators = ["MadGraph"]
theApp.finalize()
theApp.exit()

