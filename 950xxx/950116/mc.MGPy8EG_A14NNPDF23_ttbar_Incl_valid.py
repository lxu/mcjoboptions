import MadGraphControl.MadGraph_NNPDF30NLOnf4_Base_Fragment
from MadGraphControl.MadGraphUtils import *

nevents = runArgs.maxEvents*3.0 if runArgs.maxEvents>0 else 3.0*evgenConfig.nEventsPerJob

process = """
import model sm
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
generate p p > t t~
output -f"""

process_dir = new_process(process)

#Fetch default LO run_card.dat and set parameters
settings = {'lhe_version':'2.0', 
           'cut_decays':'F', 
           'nevents'       :nevents}
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)  

#### Shower 
evgenConfig.description = 'MadGraph_ttbar'
evgenConfig.keywords+=['ttbar','jets']

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
