import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *

# Nevents setting -- allow for merging losses
nevents = runArgs.maxEvents*3. if runArgs.maxEvents>0 else 3.*evgenConfig.nEventsPerJob

# Set up process
process = """
import model sm-no_b_mass
define p = g u c d s u~ c~ d~ s~ b b~
define j = g u c d s u~ c~ d~ s~ b b~
generate p p > t t~ @0
add process p p > t t~ j @1
output -f"""

process_dir = new_process(process)

#Fetch default LO run_card.dat and set parameters
settings = {'lhe_version' : '2.0',
           'cut_decays'  : 'F',
           'ickkw'       : 0,
           'drjj'        : 0.0,
           'maxjetflavor': 5,
           'ktdurham'    : 30,
           'dparameter'  : 0.4,
           'nevents'     : nevents}
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

#### Shower
evgenConfig.description = 'MadGraph_ttbar'
evgenConfig.keywords+=['ttbar','jets']

PYTHIA8_nJetMax=1
PYTHIA8_Dparameter=settings['dparameter']
PYTHIA8_Process='pp>tt~'
PYTHIA8_TMS=settings['ktdurham']
PYTHIA8_nQuarksMerge=settings['maxjetflavor']
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
include("Pythia8_i/Pythia8_CKKWL_kTMerge.py")
#Needed for ttbar:
genSeq.Pythia8.Commands+=["Merging:mayRemoveDecayProducts=on"]
