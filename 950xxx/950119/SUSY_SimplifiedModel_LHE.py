include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py' )

# These job options are designed to run MadGraph to make an LHE file and not to run a showering generator afterwards.
# Because Generate_tf requires an output file, we need to fake it a bit, which is why we run Pythia8 on the first LHE event.
evgenConfig.contact  = [ "emma.sian.kuwertz@cern.ch" ]
evt_multiplier = 1.1 # Let the number of events be set externally

from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
gentype=get_physics_short().replace('_valid','').split('SM')[1].split('_')[1]
mass_string = get_physics_short().replace('_valid','').split('_')[-1]

# Making a gridpack, or reading a gridpack?
# Note this means the two modes for these JO are make a gridpack or make LHEs from a gridpack
from MadGraphControl.MadGraphUtils import is_gen_from_gridpack
writeGridpack = not is_gen_from_gridpack()

if gentype=='GG':
    masses['1000021'] = float( mass_string )
    masses['1000022'] = 0.5 # just set to 0.5 here, will be overwritten when used for showering
    process = '''
    generate p p > go go $ susysq susysq~ @1
    add process p p > go go j $ susysq susysq~ @2
    add process p p > go go j j $ susysq susysq~ @3
    '''
    evgenConfig.keywords += ['simplifiedModel','gluino']
    evgenConfig.description = 'gluino production, m_glu = %s GeV'%(masses['1000021'])

elif gentype=='SS':
    masses['1000001'] = float( mass_string )
    masses['1000002'] = float( mass_string )
    masses['1000003'] = float( mass_string )
    masses['1000004'] = float( mass_string )
    masses['2000001'] = float( mass_string )
    masses['2000002'] = float( mass_string )
    masses['2000003'] = float( mass_string )
    masses['2000004'] = float( mass_string )
    masses['1000022'] = 0.5 # just set to 0.5 here, will be overwritten when used for showering
    process = '''
    generate p p > susysq susysq~ $ go @1
    add process p p > susysq susysq~ j $ go @2
    add process p p > susysq susysq~ j j $ go @3
    '''
    evgenConfig.keywords += ['simplifiedModel','squark']
    evgenConfig.description = 'squark production, m_squ = %s GeV'%(masses['1000001'])

include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )

# Since this is only for LHEs, make sure we pop off any unnecessary algorithms
# These are added by other pieces of the system. Adding switches to avoid them
# is a bit cumbersome.
if hasattr(genSeq,'Pythia8'): del genSeq.Pythia8
if hasattr(genSeq,'EvtInclusiveDecay'): del genSeq.EvtInclusiveDecay
if 'Pythia8' in evgenConfig.generators: evgenConfig.generators.remove('Pythia8')
if 'EvtGen' in evgenConfig.generators: evgenConfig.generators.remove('EvtGen')