include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py' )

# Set masses based on physics short
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
phys_short = get_physics_short()
masses['1000021'] = float(phys_short.split('_')[4])
masses['1000022'] = float(phys_short.split('_')[5].split('.')[0])
if masses['1000022']<0.5: masses['1000022']=0.5

# Generate gluino pairs
process = '''
generate p p > go go $ susysq susysq~ @1
add process p p > go go j $ susysq susysq~ @2
add process p p > go go j j $ susysq susysq~ @3
'''

# Set up the decays - gluino to four flavors of quarks
decays['1000021'] = """DECAY   1000021     7.40992706E-02   # gluino decays
#           BR         NDA      ID1       ID2       ID3
     2.50000000E-01    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     2.50000000E-01    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     2.50000000E-01    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     2.50000000E-01    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
"""

# Set a default event multiplier
evt_multiplier = 4

######## Filter Section #######
if 'MET100' in phys_short:
    evgenLog.info('MET 100 GeV filter is applied')
    include ( 'GeneratorFilters/MissingEtFilter.py' )
    filtSeq.MissingEtFilter.METCut = 100*GeV
    evt_multiplier = 5.

evgenConfig.contact  = [ "takashi.yamanaka@cern.ch" ]
evgenConfig.keywords += ['simplifiedModel','gluino']
evgenConfig.description = 'gluino production, glu->qq+LSP in simplified model, m_glu = %s GeV, m_N1 = %s GeV'%(masses['1000021'],masses['1000022'])

include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )

genSeq.Pythia8.Commands += ["Merging:Process = pp>{go,1000021}{go,1000021}"]