# Generator transform pre-include
#  Gets us ready for on-the-fly SUSY SM generation
include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py' )

# Set masses based on physics short
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
phys_short = get_physics_short()

# special to handle MadSpin configuration via JO name:
madspindecays = "MadSpin" in phys_short

#--------------------------------------------------------------
# MadGraph configuration
#--------------------------------------------------------------
mass_string = phys_short.split('N1_')[1]
# First the piece that is independent of heavy production
masses['1000022'] = float( mass_string.split('_')[1] ) #chi10
if masses['1000022']<0.5: masses['1000022']=0.5
heavy_mass = float( mass_string.split('_')[0] ) # heavy particle - squark or gluino
masses['1000023'] = 0.5*(heavy_mass+masses['1000022'])  #chi20 between chi10 and heavy thing
deltaM = 0.5*(heavy_mass - masses['1000022']) # (heavy mass - mchi10) / 2 = mchi20 - mchi10
if 'SL' in phys_short:
    for flav in [1,2,3,4,5,6]:
        masses[str(1000010+flav)] = 0.5*(masses['1000022']+masses['1000023'])  #sleptons and sneutrinos

if '_SS_' in phys_short:
    # Direct gluino decay to LSP (0-lepton, grid 1 last year)
    # Set 10 degenerate squarks
    for flav in [1,2,3,4,5]:
        masses[str(1000000+flav)] = heavy_mass
        masses[str(2000000+flav)] = heavy_mass
    process = '''
define susylqA = ul dl cl sl ur dr cr sr b1 b2
define susylqA~ = ul~ dl~ cl~ sl~ ur~ dr~ cr~ sr~ b1~ b2~
generate p p > susylqA susylqA~ $ go susyweak @1
add process p p > susylqA susylqA~ j $ go susyweak @2
add process p p > susylqA susylqA~ j j $ go susyweak @3
'''
    msdecaystring="""
define all = e+ e- mu+ mu- ta+ ta- u u~ d d~ c c~ s s~ b b~ ve vm vt ve~ vm~ vt~
decay n2 > all all n1
"""
    evgenConfig.keywords += ['simplifiedModel','squark','Z']
    evgenConfig.description = 'SUSY Simplified Model with squark production and decays via Z with MadGraph/Pythia8, m_squ = %s GeV, m_N2 = %s GeV, m_N1 = %s GeV'%(masses['1000001'],masses['1000023'],masses['1000022'])

elif '_GG_' in phys_short:
    # Direct gluino decay to LSP (0-lepton, grid 1 last year)
    masses['1000021'] = heavy_mass
    process = '''
generate p p > go go
add process p p > go go j
add process p p > go go j j
'''
    msdecaystring="""
define all = e+ e- mu+ mu- ta+ ta- u u~ d d~ c c~ s s~ b b~ ve vm vt ve~ vm~ vt~
decay go > jb jb n2, n2 > all all n1
"""
    evgenConfig.keywords += ['simplifiedModel','gluino', 'Z']
    evgenConfig.description = 'SUSY Simplified Model with gluino production and decays via Z with MadGraph/Pythia8, m_glu = %s GeV, m_N2 = %s GeV, m_N1 = %s GeV'%(masses['1000021'],masses['1000023'],masses['1000022'])
#--------------------------------------------------------------
# Decay configuration
#--------------------------------------------------------------
# The heavy particle always decays 100% to N2
if '_SS_' in phys_short: # Squark case, deal with left and right
    for p in [1,2,3,4,5]:
        susy_p = str(1000000+p)
        decays[susy_p]="""DECAY   %s     3.95203891E+00   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000023         %s   # BR(~d_R -> ~chi_20 d)
"""%(susy_p,str(p))
        susy_p = str(2000000+p)
        decays[susy_p]="""DECAY   %s     3.95203891E+00   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000023         %s   # BR(~d_R -> ~chi_20 d)
"""%(susy_p,str(p))
elif '_GG_' in phys_short: # Gluino case, deal with all 5 flavor decays
    decays['1000021'] = """DECAY   1000021     7.40992706E-02   # gluino decays
#           BR         NDA      ID1       ID2       ID3
     2.00000000E-01    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     2.00000000E-01    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     2.00000000E-01    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     2.00000000E-01    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     2.00000000E-01    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
"""
# The N2 decays one of three ways:
# Via intermediate sleptons
if 'SLN1' in phys_short:
    decays['1000023']="""DECAY   1000023     9.37327589E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     8.33333333E-02    2     1000011       -11   # BR(~chi_20 -> ~e_L-     e+)
     8.33333333E-02    2    -1000011        11   # BR(~chi_20 -> ~e_L+     e-)
     8.33333333E-02    2     1000013       -13   # BR(~chi_20 -> ~mu_L-    mu+)
     8.33333333E-02    2    -1000013        13   # BR(~chi_20 -> ~mu_L+    mu-)
     8.33333333E-02    2     1000015       -15   # BR(~chi_20 -> ~tau_1-   tau+)
     8.33333333E-02    2    -1000015        15   # BR(~chi_20 -> ~tau_1+   tau-)
     8.33333333E-02    2     1000012       -12   # BR(~chi_20 -> ~nu_eL    nu_eb)
     8.33333333E-02    2    -1000012        12   # BR(~chi_20 -> ~nu_eL*   nu_e )
     8.33333333E-02    2     1000014       -14   # BR(~chi_20 -> ~nu_muL   nu_mub)
     8.33333333E-02    2    -1000014        14   # BR(~chi_20 -> ~nu_muL*  nu_mu )
     8.33333333E-02    2     1000016       -16   # BR(~chi_20 -> ~nu_tau1  nu_taub)
     8.33333333E-02    2    -1000016        16   # BR(~chi_20 -> ~nu_tau1* nu_tau )
"""
    # And we need to add the slepton decays
    for l in [11,12,13,14,15,16]:
        susy_p = str(1000000+l)
        decays[susy_p]="""DECAY   %s     1.00000000E-01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        %s   # BR(~mu_L -> ~chi_10 lepton)
"""%(susy_p,str(l))
# Via an off-shell Z
elif 'offshellZN1' in phys_short:
    decays['1000023']="""DECAY   1000023     9.37327589E-04   # neutralino2 decays (offshell decay BRs are taken from Z BRs)
#          BR         NDA      ID1       ID2     (ID3)
     0.03366000E+00    3     1000022        11   -11   # BR(~chi_20 -> ~chi_10   e+ e- )
     0.03366000E+00    3     1000022        13   -13   # BR(~chi_20 -> ~chi_10   mu+ mu- )
     0.03366000E+00    3     1000022        15   -15   # BR(~chi_20 -> ~chi_10   tau+ tau- )
     0.06600000E+00    3     1000022        12   -12   # BR(~chi_20 -> ~chi_10   nu_e nu_e~ )
     0.06600000E+00    3     1000022        14   -14   # BR(~chi_20 -> ~chi_10   nu_mu nu_mu~ )
     0.06600000E+00    3     1000022        16   -16   # BR(~chi_20 -> ~chi_10   nu_tau nu_tau~ )
     0.11651000E+00    3     1000022         2   -2    # BR(~chi_20 -> ~chi_10   u ub )
     0.15600000E+00    3     1000022         1   -1    # BR(~chi_20 -> ~chi_10   d db )
     0.11651000E+00    3     1000022         4   -4    # BR(~chi_20 -> ~chi_10   c cb )
     0.15600000E+00    3     1000022         3   -3    # BR(~chi_20 -> ~chi_10   s sb )
     0.15600000E+00    3     1000022         5   -5    # BR(~chi_20 -> ~chi_10   b bb )
"""
# directly via an on-shell Z
elif 'ZN1' in phys_short:
    decays['1000023']="""DECAY   1000023     9.37327589E-04   # neutralino2 decays (offshell decay BRs are taken from Z BRs)
#          BR         NDA      ID1       ID2     (ID3)
     1.00000000E+00    2     1000022        23         # BR(~chi_20 -> ~chi_10   Z )
"""

evgenConfig.contact  = ["arka.santra@cern.ch" ]

#--------------------------------------------------------------
# Madspin configuration
#--------------------------------------------------------------
if madspindecays:
    if msdecaystring=="":
        raise RuntimeError("Asking for MadSpin decays, but no decay string provided!")
    madspin_card='madspin_card.dat'
    mscard = open(madspin_card,'w')
    mscard.write("""
set BW_cut 100                # cut on how far the particle can be off-shell
set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
set seed %i
set spinmode none
# specify the decay for the final state particles
%s
# run the actual code
launch"""%(runArgs.randomSeed,msdecaystring))
    mscard.close()

#--------------------------------------------------------------
# Filter setup
#--------------------------------------------------------------
# Two-lepton+Met filter
if '2LMET100' in phys_short:
    evt_multiplier = 100
    include('GeneratorFilters/MultiLeptonFilter.py')
    MultiLeptonFilter = filtSeq.MultiLeptonFilter
    filtSeq.MultiLeptonFilter.Ptcut = 5000.
    filtSeq.MultiLeptonFilter.Etacut = 2.8
    filtSeq.MultiLeptonFilter.NLeptons = 2
    include("GeneratorFilters/MissingEtFilter.py")
    filtSeq.MissingEtFilter.METCut = 100*GeV            # MET > 100 GeV
    filtSeq.Expression = "(MultiLeptonFilter) and (MissingEtFilter)"
# Two-lepton filter alone
elif '2L' in phys_short:
    evt_multiplier = 20
    include('GeneratorFilters/MultiLeptonFilter.py')
    MultiLeptonFilter = filtSeq.MultiLeptonFilter
    filtSeq.MultiLeptonFilter.Ptcut = 5000.
    filtSeq.MultiLeptonFilter.Etacut = 2.8
    filtSeq.MultiLeptonFilter.NLeptons = 2
# MET filter alone
elif 'MET100' in phys_short:
    evt_multiplier *= 2
    include ( 'GeneratorFilters/MissingEtFilter.py' )
    MissingEtFilter = filtSeq.MissingEtFilter
    filtSeq.MissingEtFilter.METCut = 100*GeV

#--------------------------------------------------------------
# Z->ll for low deltaM
#--------------------------------------------------------------
# For low deltaM = m(N2) - m(N1) in the ZN1 grid, if there's a Z in the event we need to use madspin for the decays
if "ZN1" in phys_short and deltaM <= 20 and not madspindecays:
    raise RuntimeError("Mass difference <20 GeV: m_N2 - m_N1 = "+str(deltaM)+"; please use MadSpin")

include('MadGraphControl/SUSY_SimplifiedModel_PostInclude.py')

genSeq.Pythia8.Commands += [ "Merging:Process = guess" ]
genSeq.Pythia8.UserHooks += ['JetMergingaMCatNLO']