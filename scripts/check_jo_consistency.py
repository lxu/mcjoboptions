#!/usr/bin/python
from __future__ import print_function
import os, re, string, subprocess, sys

# Function to check the jO naming
def check_naming(jofile):
    joparts = jofile.split(".")
    officialJO = False
    error=False
    if joparts[0].startswith("mc"):
        officialJO = True
        ## Check that the JO does not appear to be an old one, since we can't use those
        if joparts[0] != "mc":
            print("\tThe jO file name prefix should be mc not " + joparts[0])
            error=True
        else:
            print("\tOK: New jobOption file")
        ## Check that there are exactly 3 name parts separated by '.': MCxx, DSID, physicsShort, .py
        if len(joparts) != 3:
            print("\tERROR:" + jofile + " name format is wrong: must be of the form MC<xx>.<physicsShort>.py: please rename.")
            error=True
        else:
            print("\tOK: name format correct" )
                
    ## Check the length limit on the physicsShort portion of the filename
    jo_physshortpart = joparts[1]
    if len(jo_physshortpart) > 50:
        print("\tERROR: " + jo_physshortpart + " contains a physicsShort field of more than 50 characters: please rename.")
        error=True
    else:
        print("\tOK: " + jo_physshortpart + " physicsShort less than 50 characters long")

    jo_physshortparts = jo_physshortpart.split("_")
    ## There must be at least 2 physicsShort sub-parts separated by '_': gens, (tune)+PDF, and process
    if len(jo_physshortparts) < 2:
        print("\tERROR: " + jofile + " has too few physicsShort fields separated by '_': should contain <generators>(_<tune+PDF_if_available>)_<process>. Please rename.")
        error=True
    else:
        print("\tOK: 2 physicsShort parts found")

    #Where the Generator list files are located, need to change if the txt file is placed somewhere else.
    File_path = os.path.dirname(os.path.abspath(__file__))
    GenListFile = File_path+'/../common/GeneratorList.txt'
    if os.path.exists(GenListFile):
        print ("\tOK: Loading generator list file from "+ GenListFile)
    elif os.path.exists('/cvmfs/atlas.cern.ch/repo/sw/Generators/MCJobOptions/common/GeneratorList.txt'):
        print ("\tOK: Loading generator list file from cvmfs")
        GenListFile = '/cvmfs/atlas.cern.ch/repo/sw/Generators/MCJobOptions/common/GeneratorList.txt'
    else:
        print ("\tERROR: Generator list file not found!")
        error=True
    #read Generator name in a txt file in common folder. 
    Generators = {}
    with open(GenListFile) as f:
        for line in f:
            (key, val) = line.split()
            Generators[key] = val

    Generators_abb = list(Generators.values())
    if joparts[0].startswith("mc"): #< if this is an "official" JO
        genpart = jo_physshortparts[0]
        #Check No Generator full name in physicsshort. except the for some generator doesn't have short name. 
        GenFullNameNo = 0
        for Generator, Generator_abb in Generators.items():
            if genpart.find(Generator)!=-1 and Generator!=Generator_abb:
                print ("\tERROR: Generator full name "+Generator+" is found, please use abbreviation "+Generator_abb)
                GenFullNameNo += 1
        if GenFullNameNo>0:
            print ("\tERROR: %s generator full names are found! Please use abbreviation instead." %GenFullNameNo)
            error=True
        else:
            print ("\tOK: No generator full name is found")

        #Check No unrecognized generator abbreviation in physicsshort.                                                                                                                                     
        generator_list=[]
        i = 0
        while i<len(genpart):
            for step in range(1, len(genpart)+1-i):
                if genpart[i:i+step] in Generators_abb:
                    generator_list.append(genpart[i:i+step])
                    i+=step
                    break
                elif step==len(genpart)-i:
                    print ("\tERROR: Not recognised generator abbreviation " + genpart[i:i+step])
                    i+=step
                    error=True
        print ("\tGenerators used: ", generator_list)
        
        # Check if there is a dash in the filename
        if "-" in jofile:
            print("\tERROR: " + jofile + " contains a dash which is not allowed: please remove it.")
            error=True
            
        # Return the first name of the generator
        if error:
            return None
        else:
            if "valid" in jofile.lower():
                return "Val"
            else:
                return generator_list[0]

# function to get list of unique DSIDs from a specifi branch in git remote
def getDSIDInBranch(branch):
    command="git ls-tree -r --name-only {branch} | awk -F'/' '/[0-9]{{3}}xxx/ {{print $2}}' | sort -u".format(branch=branch)
    return list(map(int, filter(None,os.popen(command).read().strip().split("\n"))))
    
# function to check if a DSID is free
def isFreeDSID(dsid):
    # Get current branch
    try:
        currentBranch=os.environ["CI_COMMIT_REF_NAME"]
    except:
        command="git rev-parse --abbrev-ref HEAD"
        currentBranch=os.popen(command).read().strip()
    # Get all remote branches excluding current branch
    command="git for-each-ref --format='%(refname)' refs/remotes/origin/"
    branches=list(filter(lambda s: currentBranch not in s, os.popen(command).read().strip().split("\n")))
    for branch in branches:
        if int(dsid) in getDSIDInBranch(branch):
            print("\tERROR: {dsid} used in branch: {branch}".format(dsid=dsid, branch=branch))
            return False
    return True

# function to suggest DSID
# increment is used to handle several DSIDs
def findSmallestFreeDSID(minDSID,maxDSID,currentDSID,increment):
    command="for branch in $(git for-each-ref --format='%(refname)' refs/remotes/origin/); do git ls-tree -r --name-only $branch ; done | \
            awk -F'/' -v min=\"{minDSID}\" -v max=\"{maxDSID}\"  '/[0-9]{{3}}xxx/ {{if ($2 >= min && $2 <= max) print $2}}' | sort -u".format(minDSID=minDSID, maxDSID=maxDSID)
    occupiedDSIDs=list(map(int, filter(None,os.popen(command).read().strip().split("\n"))))
    freeDSIDs = [i for i in range(minDSID,maxDSID+1) if i not in occupiedDSIDs]
    print("\t\tERROR: Move {currentDSIDxxx}xxx/{currentDSID} -> {freeDSIDxxx}xxx/{freeDSID}"
          .format(currentDSIDxxx=currentDSID[0:3], currentDSID=currentDSID,
                  freeDSIDxxx=str(freeDSIDs[increment])[0:3], freeDSID=freeDSIDs[increment]))

# function to check the DSID range
# increment is used to handle several DSIDs
def check_dsidrange(generatorName,dsid,increment):
    # Dictionary with allowed DSID ranges
    allowedDSIDranges={"MG":   [500000, 599999],
                       "aMC":  [500000, 599999],
                       "Ph":   [600000, 699999],
                       "Sh":   [700000, 799999],
                       "Py8":  [800000, 829999],
                       "P8B":  [800000, 829999],
                       "H7":   [830000, 859999],
                       "HI":   [860000, 899999],
                       "Misc": [900000, 949999],
                       "Val":  [950000, 999999]
                       }
    # Get minimum and maximum DSID range according to generator name
    minDSID=allowedDSIDranges[generatorName][0]
    maxDSID=allowedDSIDranges[generatorName][1]
    
    # Check if DSID is in the correct range
    if int(dsid) >= minDSID and int(dsid) <= maxDSID:
        print("\tOK: correct DSID range")
        # If the DSID is already in the master branch we don't need to test if it's free
        if int(dsid) in getDSIDInBranch("refs/remotes/origin/master"):
            return 0
        else:  # Check if the DSID is not used by any other branch in remote
            if isFreeDSID(dsid):
                print("\tOK: {dsid} not used by other branches".format(dsid=dsid))
            else:
                return 1
            return 0
    else:
        print("\tERROR: DSID:{dsid}, generator:{generatorName} => allowed DSID range {minDSID} - {maxDSID}".format(dsid=dsid, generatorName=generatorName, minDSID=minDSID, maxDSID=maxDSID))
        findSmallestFreeDSID(minDSID,maxDSID,dsid,increment)
        return 1
        
# function to check that the jO file does not contain any MC15JobOptions includes
def check_mc15includes(file):
    File_path = os.path.dirname(os.path.abspath(__file__))
    jOFile = File_path+'/../'+file
    with open(jOFile) as f:
        if 'MC15JobOptions' in f.read():
            print("\tERROR: file {file} contains includes pointing to MC15JobOptions".format(file=jOFile))
            return 1
    print("\tOK: file does not contain includes pointing to MC15JobOptions")
    return 0
