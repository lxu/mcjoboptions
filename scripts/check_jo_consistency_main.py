from __future__ import print_function
import os, re, string, subprocess, sys
from check_jo_consistency import *

"""
Script to check the naming consistency of jO files.
Usage:
    1. provide list of files as command-line arguments
    2. if no argument is provided, files are obtained from git diff of current HEAD to master branch
"""

# Main script
def main():
    print("\n===> Checking jobOption consistency...\n")
    files=list()
    # If command line arguments are provided use those
    if (len(sys.argv) > 1):
        for arg in range(1,len(sys.argv)):
            files.append(sys.argv[arg])
    # Otherwise get list of files from git diff-tree command
    else:
        command="git diff-tree --name-only -r origin/master..HEAD --diff-filter=AMR | grep -E \"mc.*.py\""
        # filter list of string obtained from git diff to remove empty strings (no file modified)
        files=list(filter(None,os.popen(command).read().strip().split("\n")))
    
    error_DSIDrange, error_mc15includes = 0,0
    if files:
        # Keep track of how many DSIDs are added per generator
        newDSIDs = {"MG":  0, "aMC": 0, "Ph":  0, "Sh":  0, "Py8":  0, "P8B": 0, "H7": 0, "HI": 0, "Val": 0, "Misc": 0}
        # Loop over jO files
        for file in files:
            # Check if file exists
            if not os.path.exists(file):
                print("ERROR: file {file} does not exist!".format(file=file))
                sys.exit(1)
            # Extract naming
            dsidxxx=file.split("/")[0]
            dsiddir=file.split("/")[1]
            jofile=file.split("/")[2]
            # Check that the jO lives in a directory named like NNNxxx
            if not re.match("[0-9]{3}xxx", dsidxxx):
                print("ERROR: modified jO lives in directory " + dsidxxx + ". Need to rename directory like NNNxxx, where N are the first 3 digits of the DSID")
                error=True
            # Check that the DSID directory NNNXYZ where the jO lives is inside NNNxxx
            if dsidxxx[:3] != dsiddir[:3]:
                print("ERROR: modified jO lives in directory " + dsidxxx + "/" + dsiddir + ". Need to put it in " + dsiddir[:3] + "xxx/" + dsiddir)
                error=True
            print("\nModified jO file: {0} - jO file = {1}".format(file, jofile))
            # Get the name of the first generator and print it
            firstGenerator=check_naming(jofile)
            if firstGenerator == None:
                print("Errors occured. Exiting.")
                sys.exit(1)
            # Check that the DSID directory where the jO is corresponds to the DSID range assigned for that generator
            if firstGenerator in newDSIDs: # Most commonly used generators
                error_DSIDrange += check_dsidrange(firstGenerator, dsiddir, newDSIDs[firstGenerator])
                newDSIDs[firstGenerator] += 1
            else:
                if firstGenerator in "Hijing" "Hydjet" "HvyN" "Starlight" "AMPT" "SuperChic": # Heavy Ion generators
                    error_DSIDrange += check_dsidrange("HI", dsiddir, newDSIDs["HI"])
                    newDSIDs["HI"] += 1
                else: # Everything else
                    error_DSIDrange += check_dsidrange("Misc", dsiddir, newDSIDs["Misc"])
                    newDSIDs["Misc"] += 1
            # Check whether the file includes any MC15JobOption includes
            error_mc15includes += check_mc15includes(file)
    else:
        print("OK: no modified jO files")
        
    if error_DSIDrange != 0 or error_mc15includes != 0:
        print("\n\n===> Errors found - check output of individual checks above")
        sys.exit(1)
    else:
        print("\n\n===> All checks successfull")
        sys.exit(0)

if __name__== "__main__":
    main()
    

