#!/bin/bash

# Make sure that if a command fails the script will continue
set +e

# Get all directories in which jobOptions have been modified/added
# Explanation: git diff-tree gets the files that have been added, modified, deleted etc
# grep will look for DSID directories named like NNNxxx
# awk will split the paths by slashes and will print the path except the last field which is the filename (NF="")
# Then we only keep one such path (uniq) removing the trailing slash with sed
dirs=($(git diff-tree --name-only -r origin/master..HEAD --diff-filter=AMCR | grep -E '[0-9]{3}xxx/[0-9]{6}/mc.*.py' | awk 'BEGIN {FS=OFS="/"} {$NF="" ; print $0}' | uniq | sed 's|/$||'))

if (( ${#dirs[@]} == 0 )) ; then
  echo "No jO files added/modified since last commit"
  exit 0
fi

maxJobs=1 # Maximum number of Gen_tf jobs to run - see run_athena.sh - for now use only 1 job - eventually to be replaced with ${#dirs[@]}
jobsProcessed=0 # Keep track of how many times Gen_tf was run

# Loop over directories with new jO
for dir in "${dirs[@]}" ; do
  
  #Check if we have already processed the maximum number of jobs
  if [ "$jobsProcessed" -ge "$maxJobs" ] ; then break ; fi
  
  # There should be only 1 file called log.generate.short in each of these directories
  # This should have been uploaded by the user (running scripts/commit_new_dsid.sh locally)
  if [ ! -f $dir/log.generate.short ] ; then
    echo "WARNING: No log.generate.short in $dir"
    echo "athena running was skipped for this directory. Make sure that the requesters have tested the jO localy!!!"
    continue
  else
    echo "OK: log.generate.short found"
  fi

  # There should be only 1 file called log.generate_ci in each of these directories
  # This should have been produced as an artifact by the run_athena CI job
  if [ ! -f $dir/log.generate_ci ] ; then
    echo "ERROR: No log.generate_ci in $dir."
    echo "The run_athena CI job must have failed. Please check output of run_athena CI job."
    exit 1
  else
    echo "OK: log.generate_ci found"
  fi

  echo "Running: python scripts/logParser.py -i $dir/log.generate_ci -j $joFile -c -u ..."

  # Check output
  joFile=$(ls $dir/mc.*.py)
  python scripts/logParser.py -i $dir/log.generate_ci -j $joFile -c -u | tee log.generate_ci_out
  if [ "$?" == "1" ] ; then
    echo "ERROR: logParser execution failed. This usually implies that $dir/log.generate_ci is malformatted."
    echo "Check the output of the previous CI job."
    exit 1
  fi

  # If no error: remove log.generate file from repository
  errors=$(grep -E 'Errors.*Warnings' log.generate_ci_out | awk '{print $3}')
  rm -f log.generate_ci_out
  if (( errors > 0 )) ; then
    echo "ERROR: $dir/log.generate_ci contains errors"
    exit 1
  else
    echo "OK: logParser found no errors. Will now remove the log.generate.short file..."
    git rm -f $dir/log.generate.short
  fi

  let jobsProcessed=$jobsProcessed+1
done

# If jobsProcessed >= maxJobs it means that the loop has been completed succesfully once
# i.e. maxJobs have been run successfully in athena - no need to go again through the loop
# just delete all log.generate.short files
if [ "$jobsProcessed" -ge "$maxJobs" ] ; then
  echo "OK: athena was run only $maxJobs times. Will now remove all the other log.generate.short files"
  git rm -f `find . -name log.generate.short`
fi


exit 0
