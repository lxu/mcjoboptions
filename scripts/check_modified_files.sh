#!/bin/bash

# Make sure that if a command fails the script will continue
set +e

echo "Find files that have been modified, copied, renamed..."
pass=true

# Find number of files that have been:
# Copied (C), Deleted (D), Modified (M), Renamed (R), have their type
# (i.e. regular file, symlink, submodule, …​) changed (T), are Unmerged (U),
# are Unknown (X), or have had their pairing Broken (B).
nchanged=$(git diff --name-status origin/master..HEAD --diff-filter=MDCRTUXB | grep -v -F -f scripts/modifiable.txt | wc -l)

if [ $nchanged -eq 0 ] ; then
   echo "OK: No file modified in DSID or common directories."
else
   echo "ERROR: Your commit has modified or deleted pre-existing files: "
   git diff --name-status origin/master..HEAD --diff-filter=MDCRTUXB | grep -v -F -f scripts/modifiable.txt
   echo "This is not allowed!"
   pass=false
fi

# Find number of files that have been added in directories that
# already existed (this would also modify the physics output)
echo -e "\nWill now check whether files have been added in pre-existing DSID directories"
added=($(git diff --name-only origin/master..HEAD --diff-filter=A))
if [[ "${#added[@]}" > 0 ]] ; then
   # We also need to get a list of DSID directories present in the previous commit
   # First find the hash of the previous commit
   sha=$(git log --oneline origin/master | head -1 | awk '{print $1}')
   # Then list all the files in that commit whose path starts with ???xxx (DSID directories)
   existingDSIDs=($(git ls-tree --name-only -r $sha ???xxx | awk 'BEGIN {FS="/"} ; {printf "%s/%s\n", $1, $2}' | sort | uniq))
   # Check if any files in the current commit are added in pre-existing directories
   for file in "${added[@]}"; do
      fileDir=$(dirname $file)
      if [[ " ${existingDSIDs[@]} " =~ " ${fileDir} " ]]; then
         echo "ERROR: $file added in pre-existing DSID directory. This is not allowed!"
         pass=false
      else
         echo "OK: $file not in pre-existing DSID directory"
      fi
   done
else
  echo "OK: No file added"
fi

if [ "$pass" != true  ] ; then
  exit 1
fi

exit 0

