#!/bin/bash

# For coloured output
source scripts/helpers.sh
# Whitelist of objects that can be added to the repo
source scripts/whitelist.sh

# Defaults
git=true
newDSID=()
skipAthena=false
logParserOption=""
commitMessage=""
DEFAULTIFS=$IFS
IFS=' '

####################
# Helper functions #
####################

# Usage
printHelp() {
  printInfo -f "\nUsage:"
  echo -e "./scripts/commit_new_dsid.sh -d=DSID1,DSID2,... [-s=DSID1 -m=[skip ci] -n]"
  echo -e "DSIDx is the directory containing the jO you want to commit to git\n"
  echo -e "-d|--dsid takes as arguments a comma-separated list of DSIDs to commit\n"
  echo -e "-n|--nogit will not commit anything to the git repository. Used when we only want to create the log.generat.short files and check if eveyrthing is ok for committing\n"
  echo -e "-t|--testNevents will run logParser with the -t option, i.e. it will not check whether the number of events written in the EVNT file are equal to nEventsPerJob\n"
  echo -e "-m \"[skip ...]\" takes as arguments a comma-separated list of strings that will be added to the git commit message to control the execution of the CI jobs. Possible values: [skip ci],[skip modfiles],[skip size],[skip athena],[skip logparser]. These can be combined, e.g. -m=\"[skip modfiles],[skip athena]\"\n"
  echo -e "-h|--help prints this help message\n"
}

# A function to store the DSIDs to be committed to git
parseDSIDList() {
  # Change IFS to comma for simple reading of DSID list
  IFS=','
  for string in $1 ; do
    # If there is a "-" in the requested DSIDs interpret it as a range
    if [[ $string == *"-"* ]] ; then
      # Create the sequence of DSIDs based on the numbers on the left and right from "-"
      dsid=($(echo $string | awk 'BEGIN {FS="-"} ; {printf "%i %i\n",$1,$2}' | xargs seq | tr '\n' ','))
      newDSID=( "${newDSID[@]}" "${dsid[@]}" )
    else
      # Otherwise it's a single DSID - add it in the list of DESIDs to submit
      newDSID+=($string)
    fi
  done
  # Restore IFS
  IFS=' '
}

# A function to parse the commit message
parseCommitMessage() {
  # Change IFS to comma
  IFS=','
  # Loop through the messages
  for m in $1; do
    if [[ "$m" !=  "[skip ci]"       && \
          "$m" !=  "[skip modfiles]" && \
          "$m" !=  "[skip size]"     && \
          "$m" !=  "[skip athena]"   && \
          "$m" !=  "[skip logparser]" ]] ; then
      printError "ERROR: Uknown commit message: $m"
      printError "Possible options are: [skip ci],[skip modfiles],[skip size],[skip athena],[skip logparser]"
      return 1
    else
      commitMessage+="$m "
      if [[ "$m" == "[skip athena]" || "$m" ==  "[skip logparser]" ]] ; then
        skipAthena=true
      fi
    fi
  done
  # Restore IFS
  IFS=' '
}

####################
#   Main script    #
####################

# Get command line arguments
if [[ "$#" < 1 ]] ; then
  printError "ERROR: no arguments specified"
  printHelp
  exit 1
fi

for i in "$@" ; do
  case $i in
    -h|--help)
      printHelp
      exit 0
      ;;
    -d=*|--dsid=*)    # DSIDs to commit
      parseDSIDList "${i#*=}"
      shift
      ;;
    -t|--testNevents) # will run logParser with -t option, i.e. skip check that events written be equal to nEventsPerJob
      logParserOption=" -t "
      shift
      ;;
    -n|--nogit)       # do not commit anything to git
      git=false
      shift
      ;;
    -m=*)             # add flags in the commit message to skip certain CI jobs
      parseCommitMessage "${i#*=}"
      if [[ $? == 1 ]] ; then exit 1 ; fi
      shift
      ;;
    *)
      printError "ERROR: Unrecognised option $1"
      printHelp
      exit 2
      ;;
  esac
done

# Get name of remote for pushing
remoteName=$(git remote -v | awk '$2 ~ /atlas-physics\/pmg\/mcjoboptions.git/ && $3 == "(push)" {print $1}')
if [ -z $remoteName ] ; then
  printError "ERROR: cannot find remote name corresponding to atlas-physics/pmg/mcjoboptions.git"
  exit 3
else
  printInfo "INFO: will use following remote for pushing: $remoteName"
fi

# Update all the local branches
git fetch $remoteName --prune >& /dev/null

# Get the name of the current branch
currentBranch=$(git rev-parse --abbrev-ref HEAD)
if [[ $git == "true" && $currentBranch != "master" ]] ; then
  printError "ERROR: running from branch: $currentBranch. You must switch to the master branch before executing the script"
  exit 4
fi

# Make sure that there are no staged commits
stagedCommits=$(git diff --name-status --cached)
if [ ! -z $stagedCommits ] ; then
  printError "ERROR: you are on the master branch and have the following staged commits:"
  echo $stagedCommits
  printError "You need to unstage these commits before running the script"
  exit 5
fi

# Check git version
gitversion=$(git --version | awk '{printf "%f\n",$3}')
if [[ $gitversion < 2.0 ]] ; then
  printError "ERROR: git version < 2.0 being used. Please update to git v2.0 or higher (on lxplus do: setupATLAS; lsetup git)"
  exit 6
fi

# Check python version
pythonversion=$(python3 --version | awk '{printf "%f\n",$2}')
if [[ $pythonversion < 3.6 ]] ; then
  printError "ERROR: python version < 3.6 being used. Please update to v3.6 or higher"
  exit 7
fi


# Switch to a new git branch
if (( ${#newDSID[@]} > 0 )) ; then
  if $git ; then
    # Name of new branch
    branchName=dsid"_"$USER"_"${newDSID[0]}
    # Check if branch already exists
    git fetch --all >& /dev/null
    git branch | grep $branchName >& /dev/null && branchExists=true || branchExists=false
    # If branch exists check it out
    if $branchExists ; then
      printInfo "Branch: $branchName exists. Checking it out..."
      git checkout $branchName || exit 8
    # Otherwise create new one
    else
      printInfo "Will create new branch: $branchName"
      git checkout -b $branchName
      # A push here is needed in order to avoid wrong pipelines getting triggered
      # In the future this could be fixed by gitlab
      # (silence output to not confuse user with MR link at this stage)
      git push -u -o ci.skip $remoteName $branchName >& /dev/null
    fi
  fi
fi

# Check jO naming and DSID ranges
printInfo "Checking jO consistency and DSID ranges ..."
jOs=()
for dsid in "${newDSID[@]}" ; do
  if [ -z $(ls ${dsid:0:3}xxx/$dsid/mc.*.py 2> /dev/null) ] ; then
    printError "ERROR: Directory ${dsid:0:3}xxx/$dsid does not contain any jO files"
    exit 9
  fi
  jO=$(ls ${dsid:0:3}xxx/$dsid/mc.*.py)
  jOs+="$jO "
done
python scripts/check_jo_consistency_main.py ${jOs[@]} >& /dev/null
if [[ $? != 0 ]] ; then
  printError -f "\tERROR in jO consistency checks"
  python scripts/check_jo_consistency_main.py ${jOs[@]} | grep ERROR
  exit 10
else
  printGood -f "\t${jOs[@]} in correct DSID range"
fi

# Loop over new DSID directories to be added
for dsid in "${newDSID[@]}" ; do

  # Directory should be located in
  dsiddir=${dsid:0:3}xxx
  # Check if directory exists
  if [ ! -d $dsiddir/$dsid ] ; then
    printError "ERROR: Directory $dsiddir/$dsid does not exist"
    exit 11
  else
    printInfo "New DSID directory: $dsiddir/$dsid ..."
  fi

  # Check if they contain a log.generate file
  logFound=false
  if [ ! -f $dsiddir/$dsid/log.generate ] ; then
    printWarning -f "\tWARNING: $dsiddir/$dsid does not contain log.generate"
    printWarning -f "\tYou should athena locally and add the log.generate file in $dsiddir/$dsid"
    printWarning -f "\tThe athena and logParser jobs will not run in the CI "
  else
    printGood -f "\tOK: log.generate file found."
    logFound=true
  fi

  # If athena was run locally and the log.generate file is found in the directory to commit
  # perform some checks to make sure that the new DSIDs conform to the rules
  if $logFound ; then
    # Run the logParser and store the output in a temporary file
    tmpLogParserOut=$(mktemp -u)
    joFile=$(ls $dsiddir/$dsid/mc.*.py)
    python3 scripts/logParser.py $logParserOption -c -i $dsiddir/$dsid/log.generate -j $joFile > $tmpLogParserOut
    if (( $? != 0 )); then
      cat $tmpLogParserOut
      printError -f "\tERROR: logParser run failed."
      exit 12
    fi
     
    # Check if local log.generate file passes the logParser checks
    Nerror=$(grep -E 'Errors.*Warnings' $tmpLogParserOut | awk '{print $3}')
    if (( Nerror != 0 )) ; then
      python3 scripts/logParser.py $logParserOption -i $dsiddir/$dsid/log.generate
      printError -f "\tERROR: log.generate contains errors"
      printError -f "\tFix them before committing anything!"
      exit 13
    else
      printGood -f "\tOK: log.generate file contains no errors"
    fi

    # Extract necessary information from log.generate and put it into log.generate.short
    rm -f $dsiddir/$dsid/log.generate.short
    grep 'estimated CPU for CI job' $tmpLogParserOut >  $dsiddir/$dsid/log.generate.short
    grep 'using release' $tmpLogParserOut >> $dsiddir/$dsid/log.generate.short

    # Check if job lasts more than 1h - this will cause the pipeline to fail
    cpu=$(grep CPU $dsiddir/$dsid/log.generate.short | awk '{print $8}')
    if (( $(echo "$cpu > 1.0" | bc -l) )) ; then
      printError -f "\tERROR: CI job is expected to last more than 1h - time estimate: $cpu hours"
      printError -f "\tIt is not possible to run this job in the CI. Contact the MC software coordinators"
      exit 14
    else
      printGood -f "\tOK: CI job expected to last less than 1h - time estimate: $cpu hours"
    fi
    
    # Remove logParser output file
    rm -f $tmpLogParserOut
  fi

  # Loop over files in new DSID directory and add them to the commit
  printInfo -f "\tWill now add files to git commit"
  SAVEIFS=$IFS
  IFS=$DEFAULTIFS
  for file in $(find $dsiddir/$dsid/* -not -type d); do
    # Check if the file is in the whitelist and add it in the commit
    if checkWhiteList $file ; then
      # Special case: [skip athena] or [skip logparser] have been specified in the commit message
      # Should not commit log.generate.short
      if [[ $(basename $file) == "log.generate.short" && $skipAthena == true ]] ; then
         echo -e "\t\tSkipping: $file since the logParser CI job will not be run"
         continue
      else
        if $git ; then
          git add $file
          echo -e "\t\tAdded: $file"
        else
          echo -e "\t\tWill add: $file"
        fi
      fi
    else
      echo -e "\t\tFile: $file cannot be added to the commit. Skipping."
    fi
  done
  IFS=$SAVEIFS

done


# Push the changes
if $git ; then
  if [[ -z $(git diff --cached) ]] ; then
    printError "ERROR: nothing added to the commit. Please check error messages above."
  else
    git commit -q -m "Adding new DSID ${newDSID[0]} $commitMessage"
    printInfo -f "\nYou have prepared branch $(git rev-parse --abbrev-ref HEAD)."
    printInfo "This contains the following changes:"
    git diff --stat master HEAD
    printInfo -f "\nIf this looks good, you should now run 'git push' to upload."
  fi
fi

exit 0
