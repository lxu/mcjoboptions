#!/bin/bash

# Make sure that if a command fails the script will continue
set +e

# Get all directories in which jobOptions have been modified/added
# Explanation: git diff-tree gets the files that have been added, modified, deleted etc
# grep will look for DSID directories named like NNNxxx
# awk will split the paths by slashes and will print the path except the last field which is the filename (NF="")
# Then we only keep one such path (uniq) removing the trailing slash with sed
dirs=($(git diff-tree --name-only -r origin/master..HEAD --diff-filter=AMDCRTUXB | grep -E '[0-9]{3}xxx/[0-9]{6}/mc.*.py' | awk 'BEGIN {FS=OFS="/"} {$NF="" ; print $0}' | uniq | sed 's|/$||'))

if (( ${#dirs[@]} == 0 )) ; then
  echo "No jO files added/modified since last commit"
  exit 0
fi

ORIGDIR=$PWD

# Maximum number of Gen_tf jobs to run
maxJobs=1       # Maximum number of Gen_tf jobs to run; for now use only 1 job - eventually to be replaced with ${#dirs[@]}
jobsProcessed=0 # Keep track of how many times Gen_tf was run

# Loop over directories with new jO
for dir in "${dirs[@]}" ; do
  
  # Check if we have already processed the maximum number of jobs
  if [ "$jobsProcessed" -ge "$maxJobs" ] ; then break ; fi
  
  echo "INFO: Will run athena for $dir"
  
  DSID=$(basename $dir)
  DSIDxxx=$(dirname $dir)
  
  # Move to DSID directory (actually 1 directory up since athena won't run if we go in the same dir...)
  cd $DSIDxxx 
  
  # There should be only 1 file called log.generate.short in each of these directories
  if [ ! -f $DSID/log.generate.short ] ; then
    echo "WARNING: No log.generate.short in $dir"
    echo "athena running will be skipped for this directory, however make sure that the requesters have tested the jO localy!!!"
    continue
    # The above solution is not ideal - only temporary until we enforce running athena for all new jO
    # exit 1
  else
    echo "OK: log.generate.short found"
  fi

  # Check if the job is expected to last more than 1 hour
  cpu=$(grep CPU $DSID/log.generate.short | awk '{print $8}')
  if (( $(echo "$cpu > 1.0" | bc -l) )) ; then
    echo "ERROR: Job is expected to last more than 1h - time estimate: $cpu hours"
    echo "It is not possible to run this job in the CI. Contact the MC software coordinators"
    exit 1
  else
    echo "OK: Job expected to last less than 1h - time estimate: $cpu hours"
  fi
  echo -e "\033[0m" # switch back terminal colour to black

  # Get the release to use from the log.generate.short file
  rel=$(grep 'using release' $DSID/log.generate.short | awk '{print $NF}')
  relName=$(echo $rel | awk 'BEGIN {FS="-"} ; {print $1}')
  relVer=$(echo $rel | awk 'BEGIN {FS="-"} ; {print $2}')
  
  # Find number of events to run from jO
  nEventsPerJob=$(grep '^\s*evgenConfig.nEventsPerJob\s*=' $DSID/*.py | tail -1 | awk 'BEGIN {FS="="} ; {print $2}' | awk 'BEGIN {FS="#"} ; {print $1}')
  # If nEventsPerJob has not been specified in jO take default, ie 10k
  if [ -z $nEventsPerJob ] ; then nEventsPerJob=10000 ; fi
  # Number of events to run = max(1,0.01*nEventsPerJob)
  nEvents=$(bc <<< "if (1-0.01*$nEventsPerJob >0) 1 else 0.01*$nEventsPerJob/1")

  # Setup athena release
  source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh
  asetup $relVer,$relName
    
  # Create a temporary directory from which we will run athena
  mkdir tmp_$DSID
  cd tmp_$DSID
  TMP_DSID_DIR=$PWD

  # If there is a GRID file then copy it to the directory where we run athena
  GRIDfile=$(ls ../$DSID/*.GRID.tar.gz 2>/dev/null)
  if [ ! -z "$GRIDfile" ] ; then    
    link=$GRIDfile
    # We need to iterate here to resolve relative links
    relLink=0
    while $(test -L $link) ; do
      link=$(readlink $link)
      # Jump to the directory where the parent link is located
      cd $(dirname $link) 2> /dev/null
      let relLink=$relLink+1
      if [ $relLink -gt 10 ] ; then # This should never happen, but you never know...
        echo "ERROR: file $GRIDfile is a link that points back to itself"
        exit 1
      fi
    done
    # Go back to DSID directory where we started
    cd $TMP_DSID_DIR
    
    echo "INFO: GRID file $GRIDfile will be copied from $link"
    GRIDfilename=$(basename $link)
    # Remove the link
    rm -f $GRIDfile    
    # And copy the actual file
    if [[ "$link" == "/eos/user"* ]] ; then
      HOST=root://eosuser.cern.ch
      xrdcp $HOST/$link ../$DSID/$GRIDfilename
    elif [[ "$link" == "/eos/atlas"*  ]] ; then
      HOST=root://eosatlas.cern.ch
      xrdcp $HOST/$link ../$DSID/$GRIDfilename
    else 
      cp $link ../$DSID/$GRIDfilename
    fi
    
    if [ -f ../$DSID/$GRIDfilename ] ; then 
      echo "OK: File successfully copied"
    else
      echo "ERROR: Copying GRID file failed"
      exit 1
    fi
  fi
 
  # Run athena
  echo "Will now run Gen_tf with $nEvents events"
  set -e # to catch any error from the execution of Gen_tf
  Gen_tf.py --ecmEnergy=13000. \
  --jobConfig=../$DSID \
  --outputEVNTFile=EVNT.root \
  --maxEvents=$nEvents
  set +e # to avoid exiting from the script if any other setup command fails in the loop
  
  # Update the number of jobs processed
  let jobsProcessed=$jobsProcessed+1
  
  # Rename log.generate output from transform to log.generate_ci
  # which is uploaded as an artifact
  if [ -f log.generate ] ; then
    mv log.generate ../$DSID/log.generate_ci
  else 
    echo "ERROR: no log.generate file produced"
    exit 1
  fi

  # Move to main directory
  cd $ORIGDIR

done

exit 0

