#!/bin/bash

# Function to check if a file is in the whitelist of files that are allowed to be added in a commit
checkWhiteList() {   
    name=$(echo $1)
    if [ -f $1 -a ! -L $1 ] ; then
       # Regular file checks
        if [[ ($name =~ [0-9]{3}xxx/[0-9]{6}/.*\.py) ||
              (($name =~ [0-9]{3}xxx/[0-9]{6}/.*\.dat) && !($name =~ [0-9]{3}xxx/[0-9]{6}/.*/.*\.dat)) ||
              ($name =~ [0-9]{3}xxx/[0-9]{6}/log\.generate\.short) ||
              ($name =~ [0-9]{3}xxx/[0-9]{6}/MadGraphControl/.*\.py) ||
              ($name =~ [0-9]{3}xxx/[0-9]{6}/PowhegControl/.*\.py) ||
              ($name =~ [0-9]{3}xxx/[0-9]{6}/Herwig7_i/.*\.py) ||
              ($name =~ [0-9]{3}xxx/[0-9]{6}/Pythia8_i/.*\.py) ||
              ($name =~ [0-9]{3}xxx/[0-9]{6}/Sherpa_i/.*\.py) ||
              ($name =~ common/MadGraphControl/.*\.py) ||
              ($name =~ common/MadGraphControl/dat/.*) ||
              ($name =~ common/PowhegControl/.*\.py) ||
              ($name =~ common/Herwig7_i/.*\.py) ||
              ($name =~ common/Pythia8_i/.*\.py) ||
              ($name =~ common/Sherpa_i/.*\.py) ]] ; then
            return 0 #true
        else
            return 1 #false
        fi
    elif [ -L $1 ] ; then
        # Link checks
        if [[ ($name =~ [0-9]{3}xxx/[0-9]{6}/mc_.*TeV\..*\.GRID\.tar\.gz) ||
              ($name =~ [0-9]{3}xxx/[0-9]{6}/.*\.py) ||
              (($name =~ [0-9]{3}xxx/[0-9]{6}/.*\.dat) && !($name =~ [0-9]{3}xxx/[0-9]{6}/.*/.*\.dat)) ]] ; then
            return 0 #true
        else
            return 1 #false
        fi
    else
        echo "Unknown file/filetype: $name"
        return 1
    fi
}

